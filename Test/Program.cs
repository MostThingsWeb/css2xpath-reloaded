using System;
using css2xpath;

namespace Test {
    internal class Program {
        private static void Main(string[] args) {
            while (true) {
                string input;
                input = Console.ReadLine();
                Console.WriteLine(Converter.CSSToXPath(input));
                Console.WriteLine();
            }
        }
    }
}